<?php

namespace Tests\Browser;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ExampleTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testBasicExample()
    {
        User::factory()->create();
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
            ->assertSee('TEST');
        });
    }
}