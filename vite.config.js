import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';

export default defineConfig({
    // build: {
    //     rollupOptions: {}
    // },
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/js/app.js',
            ],
            refresh: {
                paths: [
                    ...refreshPaths,
                    'resources/routes/**',
                    'routes/**',
                    'resources/views/**',

                ],
                config: {
                    delay: 0
                }
            }
        }),
        {
            name: 'blade',
            handleHotUpdate({ file, server }) {
                if (file.endsWith('.blade.php')) {
                    server.ws.send({
                        type: 'full-reload',
                        path: '*',
                    });
                }
            },
        },
    ],
    server: {
        host: '0.0.0.0',
        port: 3000,
        strictPort: true,
        hmr: {
            clientPort: 3000,
            protocol: 'ws'
        },
        // watch: {
        //     usePolling: true,
        // },
    }
});
